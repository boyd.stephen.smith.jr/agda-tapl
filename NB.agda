data Term : Set where
  true false : Term
  if_then_else_ : Term -> Term -> Term -> Term
  zero : Term
  succ pred iszero : Term -> Term
  wrong : Term

data Eq : {A B : Set} -> A -> B -> Set where
  Refl : {A : Set}{x : A} -> Eq x x

cong : {A B : Set}{x y : A} -> {f : A -> B} -> Eq x y -> Eq (f x) (f y)
cong Refl = Refl

data NumericValue : Term -> Set where
  NVZ : NumericValue zero
  NVS : {nv : Term} -> NumericValue nv -> NumericValue (succ nv)

numericValueUnique : {t : Term} -> (nv1 nv2 : NumericValue t) -> Eq nv1 nv2
numericValueUnique NVZ NVZ = Refl
numericValueUnique (NVS nv1) (NVS nv2) = cong (numericValueUnique nv1 nv2)

data Value : Term -> Set where
  ValT : Value true
  ValF : Value false
  ValN : {nv : Term} -> NumericValue nv -> Value nv

valueUnique : {t : Term} -> (v1 v2 : Value t) -> Eq v1 v2
valueUnique ValT ValT = Refl
valueUnique ValT (ValN ())
valueUnique ValF ValF = Refl
valueUnique ValF (ValN ())
valueUnique (ValN ()) ValT
valueUnique (ValN ()) ValF
valueUnique (ValN nv1) (ValN nv2) = cong (numericValueUnique nv1 nv2)

data BadBool : Term -> Set where
  BadBoolW : BadBool wrong
  BadBoolN : {nv : Term} -> NumericValue nv -> BadBool nv

badBoolUnique : {t : Term} -> (bb1 bb2 : BadBool t) -> Eq bb1 bb2
badBoolUnique BadBoolW BadBoolW = Refl
badBoolUnique BadBoolW (BadBoolN ())
badBoolUnique (BadBoolN ()) BadBoolW
badBoolUnique (BadBoolN bn1) (BadBoolN bn2) = cong (numericValueUnique bn1 bn2)

trueNotBad : {A : Set} -> BadBool true -> A
trueNotBad (BadBoolN ())

falseNotBad : {A : Set} -> BadBool false -> A
falseNotBad (BadBoolN ())

data BadNat : Term -> Set where
  BadNatW : BadNat wrong
  BadNatT : BadNat true
  BadNatF : BadNat false

badNatUnique : {t : Term} -> (bn1 bn2 : BadNat t) -> Eq bn1 bn2
badNatUnique BadNatW BadNatW = Refl
badNatUnique BadNatT BadNatT = Refl
badNatUnique BadNatF BadNatF = Refl

numericNotBad : {A : Set} -> {nv : Term} -> NumericValue nv -> BadNat nv -> A
numericNotBad NVZ ()
numericNotBad (NVS nvp) ()

data SmallStep : Term -> Term -> Set where
  If : {t1 u1 t2 t3 : Term} -> SmallStep t1 u1
    -> SmallStep (if t1 then t2 else t3) (if u1 then t2 else t3)
  IfTrue  : {t2 t3 : Term} -> SmallStep (if true  then t2 else t3) t2
  IfFalse : {t2 t3 : Term} -> SmallStep (if false then t2 else t3) t3
  IfWrong : {t1 t2 t3 : Term} -> BadBool t1
         -> SmallStep (if t1 then t2 else t3) wrong
  Succ      : {t u : Term} -> SmallStep t u -> SmallStep (succ t) (succ u)
  SuccWrong : {t : Term}   -> BadNat t      -> SmallStep (succ t) wrong
  Pred      : {t u : Term} -> SmallStep t u ->   SmallStep (pred t) (pred u)
  PredZero  :                                    SmallStep (pred zero) zero
  PredSucc  : {nv : Term}  -> NumericValue nv -> SmallStep (pred (succ nv)) nv
  PredWrong : {t : Term}   -> BadNat t ->        SmallStep (pred t) wrong
  IsZero : {t u : Term} -> SmallStep t u -> SmallStep (iszero t) (iszero u)
  IsZeroSucc : {nv : Term} -> NumericValue nv
            -> SmallStep (iszero (succ nv)) false
  IsZeroZero : SmallStep (iszero zero) true
  IsZeroWrong : {t : Term} -> BadNat t -> SmallStep (iszero t) wrong

ssNoValue : {A : Set}{t u : Term} -> Value t -> SmallStep t u -> A
ssNoValue ValT ()
ssNoValue ValF ()
ssNoValue (ValN NVZ) ()
ssNoValue (ValN (NVS x)) (Succ ss) = ssNoValue (ValN x) ss
ssNoValue (ValN (NVS x)) (SuccWrong bnp) = numericNotBad x bnp

ssNoWrong : {A : Set}{t : Term} -> SmallStep wrong t -> A
ssNoWrong ()

ssNoBadNat : {A : Set}{t u : Term} -> BadNat t -> SmallStep t u -> A
ssNoBadNat BadNatW = ssNoWrong
ssNoBadNat BadNatT = ssNoValue ValT
ssNoBadNat BadNatF = ssNoValue ValF

ssNoBadBool : {A : Set}{t u : Term} -> BadBool t -> SmallStep t u -> A
ssNoBadBool BadBoolW = ssNoWrong
ssNoBadBool (BadBoolN nv) = ssNoValue (ValN nv)

record Pair (A B : Set) : Set where
 constructor MkPair
 field
  fst : A
  snd : B

ssUniqueP : {t u v : Term} -> (ss1 : SmallStep t u) -> (ss2 : SmallStep t v)
         -> Pair (Eq u v) (Eq ss1 ss2)
ssUniqueP (If ss1) (If ss2) with (ssUniqueP ss1 ss2)
...                            | MkPair Refl prf = MkPair Refl (cong prf)
ssUniqueP (If ss) IfTrue = ssNoValue ValT ss
ssUniqueP (If ss) IfFalse = ssNoValue ValF ss
ssUniqueP (If ss) (IfWrong bb) = ssNoBadBool bb ss
ssUniqueP IfTrue (If ss) = ssNoValue ValT ss
ssUniqueP IfTrue IfTrue = MkPair Refl Refl
ssUniqueP IfTrue (IfWrong bb) = trueNotBad bb
ssUniqueP IfFalse (If ss) = ssNoValue ValF ss
ssUniqueP IfFalse IfFalse = MkPair Refl Refl
ssUniqueP IfFalse (IfWrong bb) = falseNotBad bb
ssUniqueP (IfWrong bb) (If ss) = ssNoBadBool bb ss
ssUniqueP (IfWrong bb) IfTrue = trueNotBad bb
ssUniqueP (IfWrong bb) IfFalse = falseNotBad bb
ssUniqueP (IfWrong bb1) (IfWrong bb2) = MkPair Refl (cong (badBoolUnique bb1 bb2))
ssUniqueP (Succ ss1) (Succ ss2) with (ssUniqueP ss1 ss2)
...                                | MkPair Refl prf = MkPair Refl (cong prf)
ssUniqueP (Succ ss) (SuccWrong bn) = ssNoBadNat bn ss
ssUniqueP (SuccWrong bn) (Succ ss) = ssNoBadNat bn ss
ssUniqueP (SuccWrong bn1) (SuccWrong bn2) = MkPair Refl (cong (badNatUnique bn1 bn2))
ssUniqueP (Pred ss1) (Pred ss2) with (ssUniqueP ss1 ss2)
...                                | MkPair Refl prf = MkPair Refl (cong prf)
ssUniqueP (Pred ss) PredZero = ssNoValue (ValN NVZ) ss
ssUniqueP (Pred ss) (PredSucc x) = ssNoValue (ValN (NVS x)) ss
ssUniqueP (Pred ss) (PredWrong x) = ssNoBadNat x ss
ssUniqueP PredZero (Pred ss) = ssNoValue (ValN NVZ) ss
ssUniqueP PredZero PredZero = MkPair Refl Refl
ssUniqueP PredZero (PredWrong bnp) = numericNotBad NVZ bnp
ssUniqueP (PredSucc nv) (Pred ss) = ssNoValue (ValN (NVS nv)) ss
ssUniqueP (PredSucc nv1) (PredSucc nv2) with (numericValueUnique nv1 nv2)
...                                        | prf = MkPair Refl (cong prf)
ssUniqueP (PredSucc nv) (PredWrong bn) = numericNotBad (NVS nv) bn
ssUniqueP (PredWrong bn) (Pred ss) = ssNoBadNat bn ss
ssUniqueP (PredWrong bn) PredZero = numericNotBad NVZ bn
ssUniqueP (PredWrong bn) (PredSucc nv) = numericNotBad (NVS nv) bn
ssUniqueP (PredWrong bn1) (PredWrong bn2) = MkPair Refl (cong (badNatUnique bn1 bn2))
ssUniqueP (IsZero ss1) (IsZero ss2) with (ssUniqueP ss1 ss2)
...                                    | MkPair Refl prf = MkPair Refl (cong prf)
ssUniqueP (IsZero ss) (IsZeroSucc nv) = ssNoValue (ValN (NVS nv)) ss
ssUniqueP (IsZero ss) IsZeroZero = ssNoValue (ValN NVZ) ss
ssUniqueP (IsZero ss) (IsZeroWrong bn) = ssNoBadNat bn ss
ssUniqueP (IsZeroSucc nv) (IsZero ss) = ssNoValue (ValN (NVS nv)) ss
ssUniqueP (IsZeroSucc nv1) (IsZeroSucc nv2) = MkPair Refl (cong (numericValueUnique nv1 nv2))
ssUniqueP (IsZeroSucc nv) (IsZeroWrong bn) = numericNotBad (NVS nv) bn
ssUniqueP IsZeroZero (IsZero ss2) = ssNoValue (ValN NVZ) ss2
ssUniqueP IsZeroZero IsZeroZero = MkPair Refl Refl
ssUniqueP IsZeroZero (IsZeroWrong bnp) = numericNotBad NVZ bnp
ssUniqueP (IsZeroWrong bn) (IsZero ss) = ssNoBadNat bn ss
ssUniqueP (IsZeroWrong bn) (IsZeroSucc nv) = numericNotBad (NVS nv) bn
ssUniqueP (IsZeroWrong bn) IsZeroZero = numericNotBad NVZ bn
ssUniqueP (IsZeroWrong bn1) (IsZeroWrong bn2) = MkPair Refl (cong (badNatUnique bn1 bn2))

ssUniqueDest : {t u v : Term} -> (ss1 : SmallStep t u) -> (ss2 : SmallStep t v) -> Eq u v
ssUniqueDest ss1 ss2 = Pair.fst (ssUniqueP ss1 ss2)

ssUnique : {t u v : Term} -> (ss1 : SmallStep t u) -> (ss2 : SmallStep t v) -> Eq ss1 ss2
ssUnique ss1 ss2 = Pair.snd (ssUniqueP ss1 ss2)

data DoSS : Term -> Set where
  Done : {t : Term} -> Value t -> DoSS t
  Step : {t : Term} -> (u : Term) -> SmallStep t u -> DoSS t
  Error : DoSS wrong

doSmallStep : (t : Term) -> DoSS t
doSmallStep true = Done ValT
doSmallStep false = Done ValF
doSmallStep (if t1 then t2 else t3) with (doSmallStep t1)
doSmallStep (if .true then t2 else t3) | Done ValT = Step t2 IfTrue
doSmallStep (if .false then t2 else t3) | Done ValF = Step t3 IfFalse
doSmallStep (if t1 then t2 else t3) | Done (ValN nv) = Step wrong (IfWrong (BadBoolN nv))
doSmallStep (if t1 then t2 else t3) | Step u ss = Step (if u then t2 else t3) (If ss)
doSmallStep (if .wrong then t2 else t3) | Error = Step wrong (IfWrong BadBoolW)
doSmallStep zero = Done (ValN NVZ)
doSmallStep (succ t) with (doSmallStep t)
doSmallStep (succ .true) | Done ValT = Step wrong (SuccWrong BadNatT)
doSmallStep (succ .false) | Done ValF = Step wrong (SuccWrong BadNatF)
doSmallStep (succ t) | Done (ValN nv) = Done (ValN (NVS nv))
doSmallStep (succ t) | Step u ss = Step (succ u) (Succ ss)
doSmallStep (succ .wrong) | Error = Step wrong (SuccWrong BadNatW)
doSmallStep (pred t) with (doSmallStep t)
doSmallStep (pred .true) | Done ValT = Step wrong (PredWrong BadNatT)
doSmallStep (pred .false) | Done ValF = Step wrong (PredWrong BadNatF)
doSmallStep (pred .zero) | Done (ValN NVZ) = Step zero PredZero
doSmallStep (pred (succ nv)) | Done (ValN (NVS nvp)) = Step nv (PredSucc nvp)
doSmallStep (pred t) | Step u x = Step (pred u) (Pred x)
doSmallStep (pred .wrong) | Error = Step wrong (PredWrong BadNatW)
doSmallStep (iszero t) with (doSmallStep t)
doSmallStep (iszero .true) | Done ValT = Step wrong (IsZeroWrong BadNatT)
doSmallStep (iszero .false) | Done ValF = Step wrong (IsZeroWrong BadNatF)
doSmallStep (iszero .zero) | Done (ValN NVZ) = Step true IsZeroZero
doSmallStep (iszero _) | Done (ValN (NVS nv)) = Step false (IsZeroSucc nv)
doSmallStep (iszero t) | Step u x = Step (iszero u) (IsZero x)
doSmallStep (iszero .wrong) | Error = Step wrong (IsZeroWrong BadNatW)
doSmallStep wrong = Error

smallStep : Term -> Term
smallStep t with (doSmallStep t)
smallStep t | Done x = t
smallStep t | Step u x = u
smallStep .wrong | Error = wrong

data Result : Term -> Set where
  Success : (t : Term) -> Value t -> Result t
  Failure : Result wrong

record Sigma (A : Set) (P : A -> Set) : Set where
 constructor MkSigma
 field
  witness : A
  proof : P witness

{-# TERMINATING #-}
evalSmallStep : (t : Term) -> Sigma Term Result
evalSmallStep t with (doSmallStep t)
evalSmallStep t | Done v = MkSigma t (Success t v)
evalSmallStep t | Step u ss = evalSmallStep u
evalSmallStep .wrong | Error = MkSigma wrong Failure

eval : Term -> Term
eval t with (evalSmallStep t)
eval t | MkSigma witness proof = witness

data BigStep : Term -> Term -> Set where
  BValue : {v : Term} -> Value v -> BigStep v v
  BIfTrue : {t1 t2 t3 v : Term} -> BigStep t1 true -> BigStep t2 v
         -> BigStep (if t1 then t2 else t3) v
  BIfFalse : {t1 t2 t3 v : Term} -> BigStep t1 false -> BigStep t3 v
          -> BigStep (if t1 then t2 else t3) v
  BSucc : {t nv : Term} -> NumericValue nv -> BigStep t nv -> BigStep (succ t) (succ nv)
  BPredZero : {t : Term} -> BigStep t zero -> BigStep (pred t) zero
  BPredSucc : {t nv : Term} -> NumericValue nv -> BigStep t (succ nv) -> BigStep (pred t) nv
  BIsZeroZero : {t : Term} -> BigStep t zero -> BigStep (iszero t) true
  BIsZeroSucc : {t nv : Term} -> NumericValue nv -> BigStep t (succ nv) -> BigStep (iszero t) false
  BWrong : BigStep wrong wrong
  BIfWrong : {t1 t2 t3 bb : Term} -> BadBool bb -> BigStep t1 bb
          -> BigStep (if t1 then t2 else t3) wrong
  BSuccWrong : {t bn : Term} -> BadNat bn -> BigStep t bn -> BigStep (succ t) wrong
  BPredWrong : {t bn : Term} -> BadNat bn -> BigStep t bn -> BigStep (pred t) wrong
  BIsZeroWrong : {t bn : Term} -> BadNat bn -> BigStep t bn -> BigStep (iszero t) wrong

bsUniqueDest : {t u v : Term} -> (bs1 : BigStep t u) -> (bs2 : BigStep t v) -> Eq u v
bsUniqueDest (BValue _) (BValue _) = Refl
bsUniqueDest (BValue (ValN ())) (BIfTrue _ _)
bsUniqueDest (BValue (ValN ())) (BIfFalse _ _)
bsUniqueDest (BValue (ValN (NVS nvp))) (BSucc _ bs) = cong (bsUniqueDest (BValue (ValN nvp)) bs)
bsUniqueDest (BValue (ValN ())) (BPredZero _)
bsUniqueDest (BValue (ValN ())) (BPredSucc _ _)
bsUniqueDest (BValue (ValN ())) (BIsZeroZero _)
bsUniqueDest (BValue (ValN ())) (BIsZeroSucc _ _)
bsUniqueDest (BValue (ValN ())) BWrong
bsUniqueDest (BValue (ValN ())) (BIfWrong _ _)
bsUniqueDest (BValue (ValN (NVS nvp))) (BSuccWrong bnp bs) with (bsUniqueDest (BValue (ValN nvp)) bs)
bsUniqueDest (BValue (ValN (NVS nvp))) (BSuccWrong bnp bs)    | Refl = numericNotBad nvp bnp
bsUniqueDest (BValue (ValN ())) (BPredWrong _ _)
bsUniqueDest (BValue (ValN ())) (BIsZeroWrong _ _)
bsUniqueDest (BIfTrue _ _) (BValue (ValN ()))
bsUniqueDest (BIfTrue _ bs2) (BIfTrue _ bs4) = bsUniqueDest bs2 bs4
bsUniqueDest (BIfTrue bs1 _) (BIfFalse bs3 _) with (bsUniqueDest bs1 bs3)
bsUniqueDest (BIfTrue bs1 _) (BIfFalse bs3 _)    | ()
bsUniqueDest (BIfTrue bs1 _) (BIfWrong bbp bs3) with (bsUniqueDest bs1 bs3) 
bsUniqueDest (BIfTrue bs1 _) (BIfWrong bbp bs3)    | Refl = trueNotBad bbp
bsUniqueDest (BIfFalse bs1 bs2) (BValue (ValN ()))
bsUniqueDest (BIfFalse bs1 _) (BIfTrue bs3 _) with (bsUniqueDest bs1 bs3)
bsUniqueDest (BIfFalse bs1 _) (BIfTrue bs3 _)    | ()
bsUniqueDest (BIfFalse _ bs2) (BIfFalse _ bs4) = bsUniqueDest bs2 bs4
bsUniqueDest (BIfFalse bs1 _) (BIfWrong bbp bs3) with (bsUniqueDest bs1 bs3)
bsUniqueDest (BIfFalse bs1 x) (BIfWrong bbp bs3)    | Refl = falseNotBad bbp
bsUniqueDest (BSucc _ bs) (BValue (ValN (NVS nvp))) = cong (bsUniqueDest bs (BValue (ValN nvp)))
bsUniqueDest (BSucc _ bs1) (BSucc _ bs2) = cong (bsUniqueDest bs1 bs2)
bsUniqueDest (BSucc nvp bs1) (BSuccWrong bnp bs2) with (bsUniqueDest bs1 bs2)
bsUniqueDest (BSucc nvp bs1) (BSuccWrong bnp bs2)    | Refl = numericNotBad nvp bnp
bsUniqueDest (BPredZero _) (BValue (ValN ()))
bsUniqueDest (BPredZero bs1) (BPredZero bs2) = bsUniqueDest bs1 bs2
bsUniqueDest (BPredZero bs1) (BPredSucc _ bs2) with (bsUniqueDest bs1 bs2)
bsUniqueDest (BPredZero bs1) (BPredSucc _ bs2)    | ()
bsUniqueDest (BPredZero bs1) (BPredWrong bnp bs2) with (bsUniqueDest bs1 bs2)
bsUniqueDest (BPredZero bs1) (BPredWrong bnp bs2)    | Refl = numericNotBad NVZ bnp
bsUniqueDest (BPredSucc _ _) (BValue (ValN ()))
bsUniqueDest (BPredSucc _ bs1) (BPredZero bs2) with (bsUniqueDest bs1 bs2)
bsUniqueDest (BPredSucc _ bs1) (BPredZero bs2)    | ()
bsUniqueDest (BPredSucc _ bs1) (BPredSucc _ bs2) with (bsUniqueDest bs1 bs2)
bsUniqueDest (BPredSucc _ bs1) (BPredSucc _ bs2)    | Refl = Refl
bsUniqueDest (BPredSucc nvp bs1) (BPredWrong bnp bs2) with (bsUniqueDest bs1 bs2)
bsUniqueDest (BPredSucc nvp bs1) (BPredWrong bnp bs2)    | Refl = numericNotBad (NVS nvp) bnp
bsUniqueDest (BIsZeroZero bs1) (BValue (ValN ()))
bsUniqueDest (BIsZeroZero bs1) (BIsZeroZero bs2) = Refl
bsUniqueDest (BIsZeroZero bs1) (BIsZeroSucc _ bs2) with (bsUniqueDest bs1 bs2)
bsUniqueDest (BIsZeroZero bs1) (BIsZeroSucc _ bs2)    | ()
bsUniqueDest (BIsZeroZero bs1) (BIsZeroWrong bnp bs2) with (bsUniqueDest bs1 bs2)
bsUniqueDest (BIsZeroZero bs1) (BIsZeroWrong bnp bs2)    | Refl = numericNotBad NVZ bnp
bsUniqueDest (BIsZeroSucc _ _) (BValue (ValN ()))
bsUniqueDest (BIsZeroSucc _ bs1) (BIsZeroZero bs2) with (bsUniqueDest bs1 bs2)
bsUniqueDest (BIsZeroSucc _ bs1) (BIsZeroZero bs2)    | ()
bsUniqueDest (BIsZeroSucc _ _) (BIsZeroSucc _ _) = Refl
bsUniqueDest (BIsZeroSucc nvp bs1) (BIsZeroWrong bnp bs2) with (bsUniqueDest bs1 bs2)
bsUniqueDest (BIsZeroSucc nvp bs1) (BIsZeroWrong bnp bs2)    | Refl = numericNotBad (NVS nvp) bnp
bsUniqueDest BWrong (BValue (ValN ()))
bsUniqueDest BWrong BWrong = Refl
bsUniqueDest (BIfWrong bbp bs1) (BValue (ValN ()))
bsUniqueDest (BIfWrong bbp bs1) (BIfTrue bs2 _) with (bsUniqueDest bs1 bs2)
bsUniqueDest (BIfWrong bbp bs1) (BIfTrue bs2 _)    | Refl = trueNotBad bbp
bsUniqueDest (BIfWrong bbp bs1) (BIfFalse bs2 _) with (bsUniqueDest bs1 bs2)
bsUniqueDest (BIfWrong bbp bs1) (BIfFalse bs2 _)    | Refl = falseNotBad bbp
bsUniqueDest (BIfWrong _ _) (BIfWrong _ _) = Refl
bsUniqueDest (BSuccWrong bnp bs1) (BValue (ValN (NVS nvp))) with (bsUniqueDest bs1 (BValue (ValN nvp)))
bsUniqueDest (BSuccWrong bnp bs1) (BValue (ValN (NVS nvp)))    | Refl = numericNotBad nvp bnp
bsUniqueDest (BSuccWrong bnp bs1) (BSucc nvp bs2) with (bsUniqueDest bs1 bs2)
bsUniqueDest (BSuccWrong bnp bs1) (BSucc nvp bs2)    | Refl = numericNotBad nvp bnp
bsUniqueDest (BSuccWrong _ _) (BSuccWrong _ _) = Refl
bsUniqueDest (BPredWrong _ _) (BValue (ValN ()))
bsUniqueDest (BPredWrong bnp bs1) (BPredZero bs2) with (bsUniqueDest bs1 bs2)
bsUniqueDest (BPredWrong bnp bs1) (BPredZero bs2)    | Refl = numericNotBad NVZ bnp
bsUniqueDest (BPredWrong bnp bs1) (BPredSucc nvp bs2) with (bsUniqueDest bs1 bs2)
bsUniqueDest (BPredWrong bnp bs1) (BPredSucc nvp bs2)    | Refl = numericNotBad (NVS nvp) bnp
bsUniqueDest (BPredWrong _ _) (BPredWrong _ _) = Refl
bsUniqueDest (BIsZeroWrong _ _) (BValue (ValN ()))
bsUniqueDest (BIsZeroWrong bnp bs1) (BIsZeroZero bs2) with (bsUniqueDest bs1 bs2)
bsUniqueDest (BIsZeroWrong bnp bs1) (BIsZeroZero bs2) | Refl = numericNotBad NVZ bnp
bsUniqueDest (BIsZeroWrong bnp bs1) (BIsZeroSucc nvp bs2) with (bsUniqueDest bs1 bs2)
bsUniqueDest (BIsZeroWrong bnp bs1) (BIsZeroSucc nvp bs2) | Refl = numericNotBad (NVS nvp) bnp
bsUniqueDest (BIsZeroWrong _ _) (BIsZeroWrong _ _) = Refl
