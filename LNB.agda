{-# OPTIONS --without-K #-}
module LNB where

{- {{{ METATHEORY -}
data Void : Set where

absurd : Void -> {A : Set} -> A
absurd ()

data Unit : Set where
  trivial : Unit

Not : Set -> Set
Not x = x -> Void

{-| Booleans, with short constructor names. -}
data Bool : Set where
  T F : Bool
{-# BUILTIN BOOL  Bool #-}
{-# BUILTIN TRUE  T    #-}
{-# BUILTIN FALSE F    #-}

data Dec (prop : Set) : Set where
  Yes : (proof : prop)     -> Dec prop
  No : (contra : Not prop) -> Dec prop

{-| Naturals, with constructor names ala Idris -}
data Nat : Set where
  Z : Nat
  S : Nat -> Nat
{-# BUILTIN NATURAL Nat #-}

{-| Indexes less than a number -}
data Fin : Nat -> Set where
  FZ : {n : Nat}          -> Fin (S n)
  FS : {n : Nat} -> Fin n -> Fin (S n)

{-| Optional values -}
data Maybe (A : Set) : Set where
  Nothing :   Maybe A
  Just : A -> Maybe A

{-| External choice -}
data Either (A B : Set) : Set where
  Left  : A -> Either A B
  Right : B -> Either A B

{-| Non-dependent pair -}
record Pair (A B : Set) : Set where
 constructor MkPair
 field
  fst : A
  snd : B

{-| Existential -}
record Sigma (A : Set) (P : A -> Set) : Set where
 constructor MkSigma
 field
  witness : A
  proof : P witness

{-| "For example" / Sigma by example -}
eg : {A : Set}{P : A -> Set}{x : A} -> P x -> Sigma A P
eg ex = MkSigma _ ex

data Eq {A : Set} (x : A) : A -> Set where
  Refl : Eq x x

{-| Symmetry -}
symAt : {A : Set}(x y : A)(eq : Eq x y) -> Eq y x
symAt _ _ Refl = Refl

sym : {A : Set}{x y : A}(eq : Eq x y) -> Eq y x
sym = symAt _ _

symAtSpec : {A : Set}{x : A} -> Eq (symAt x x Refl) Refl
symAtSpec = Refl

symSpec : {A : Set}{x : A} -> Eq sym (symAt x x)
symSpec = Refl

{-| Transitivity -}
transAt : {A : Set}(x y z : A) -> Eq x y -> Eq y z -> Eq x z
transAt _ _ _ Refl Refl = Refl

trans : {A : Set}{x y z : A} -> Eq x y -> Eq y z -> Eq x z
trans = transAt _ _ _

transAtSpec : {A : Set}{x : A} -> Eq (transAt x x x Refl Refl) Refl
transAtSpec = Refl

transSpec : {A : Set}{x y z : A} -> Eq trans (transAt x y z)
transSpec = Refl

{-| Congrence -}
congAppAt : {A B : Set}(f g : A -> B)(x y : A)(eqf : Eq f g)(eqa : Eq x y) -> Eq (f x) (g y)
congAppAt _ _ _ _ Refl Refl = Refl

congApp : {A B : Set}{f g : A -> B}{x y : A}(eqf : Eq f g)(eqa : Eq x y) -> Eq (f x) (g y)
congApp = congAppAt _ _ _ _

congAppAtSpec : {A B : Set}{f : A -> B}{x : A} -> Eq (congAppAt f f x x Refl Refl) Refl
congAppAtSpec = Refl

congAppSpec : {A B : Set}{f : A -> B}{x : A} -> Eq congApp (congAppAt f f x x)
congAppSpec = Refl

cong : {A B : Set}{f : A -> B}{x y : A} -> Eq x y -> Eq (f x) (f y)
cong = congApp Refl

congArg : {A B : Set}{f g : A -> B}{x : A}(eq : Eq f g) -> Eq (f x) (g x)
congArg eq = congApp eq Refl

symSymId : {A : Set}{x y : A}(eq : Eq x y) -> Eq (sym (sym eq)) eq
symSymId Refl = trans (congApp symSpec (trans (congArg symSpec) symAtSpec)) symAtSpec

{-| Congrence, binary -}
congApp2 : {A B C : Set}{f g : A -> B -> C}{x y : A}{z w : B}
        -> (eqf : Eq f g)(eqa : Eq x y)(eqb : Eq z w) -> Eq (f x z) (g y w)
congApp2 eqf eqa = congApp (congApp eqf eqa)

cong2 : {A B C : Set}{f : A -> B -> C}{x y : A}{z w : B} -> Eq x y -> Eq z w -> Eq (f x z) (f y w)
cong2 = congApp2 Refl

congArg2 : {A B C : Set}{f g : A -> B -> C}{x : A}{y : B}(eq : Eq f g) -> Eq (f x y) (g x y)
congArg2 eq = congApp2 eq Refl Refl

congApp2Arg0 : {A B C : Set}{f g : A -> B -> C}{x : A}{y z : B}
        -> (eqf : Eq f g)(eqb : Eq y z) -> Eq (f x y) (g x z)
congApp2Arg0 eqf = congApp2 eqf Refl

{-| Congruence, trinary -}
cong3 : {A B C D : Set}{f : A -> B -> C -> D}{x1 y1 : A}{x2 y2 : B}{x3 y3 : C}
     -> Eq x1 y1 -> Eq x2 y2 -> Eq x3 y3
     -> Eq (f x1 x2 x3) (f y1 y2 y3)
cong3 Refl = cong2

transReflL : {A : Set}{x y : A}(r : Eq x y) -> Eq (trans Refl r) r
transReflL Refl = trans (congArg2 transSpec) transAtSpec

transReflR : {A : Set}{x y : A}(l : Eq x y) -> Eq (trans l Refl) l
transReflR Refl = trans (congArg2 transSpec) transAtSpec

transSymRefl : {A : Set}{x y : A}(eq : Eq x y) -> Eq (trans eq (sym eq)) Refl
transSymRefl Refl = trans (congApp2Arg0 transSpec (congArg symSpec)) transAtSpec

transAssoc : {A : Set}{w x y z : A}(l : Eq w x)(c : Eq x y)(r : Eq y z) -> Eq (trans (trans l c) r) (trans l (trans c r))
transAssoc Refl Refl Refl = Refl

substAt : {A : Set}(x y : A)(eq : Eq x y) -> (P : A -> Set)(p : P x) -> P y
substAt _ _ Refl P p = p

subst : {A : Set}{x y : A}(eq : Eq x y) -> (P : A -> Set)(p : P x) -> P y
subst = substAt _ _

substSpecAt : {A : Set}(x : A){P : A -> Set}{p : P x} -> Eq (substAt x x Refl P p) p
substSpecAt _ = Refl

FinS : {n : Nat} -> (i : Fin n) -> Sigma Nat \{ m -> Eq n (S m) }
FinS FZ     = eg Refl
FinS (FS i) = eg Refl

{- METATHEORY }}} -}

{-| Types -}
data Type : Set where
  Boolean Natural : Type -- Book uses Nat instead of Natural, but conflicts for us.
  Arr : Type -> Type -> Type

decEqType : (tyx tyy : Type) -> Dec (Eq tyx tyy)
decEqType Boolean Boolean = Yes Refl
decEqType Boolean Natural = No \()
decEqType Boolean (Arr _ _) = No \()
decEqType Natural Boolean = No \()
decEqType Natural Natural = Yes Refl
decEqType Natural (Arr _ _) = No \()
decEqType (Arr _ _) Boolean = No \()
decEqType (Arr _ _) Natural = No \()
decEqType (Arr tyxa tyxr) (Arr tyya tyyr) with decEqType tyxa tyya
decEqType (Arr tyxa tyxr) (Arr tyya tyyr) | No contra = No \{Refl -> contra Refl}
decEqType (Arr tyxa tyxr) (Arr tyya tyyr) | Yes prf  with decEqType tyxr tyyr
decEqType (Arr tyxa tyxr) (Arr tyya tyyr) | Yes prf  | No contra = No \{ Refl -> contra Refl }
decEqType (Arr tyxa tyxr) (Arr tyya tyyr) | Yes prfa | Yes prfr = Yes (cong2 prfa prfr)

axiomJ : {A : Set} (C : (x y : A) -> Eq x y -> Set) -> ((x : A) -> C x x Refl)
     -> (x y : A) (prf : Eq x y) -> C x y prf
axiomJ C base x _ Refl = base x

decUIPloopBig : {A : Set}(decEq : (x y : A) -> Dec (Eq x y))(x y : A)(eq : Eq x y) -> Set
decUIPloopBig decEq x y eq with decEq x y | decEq y y
decUIPloopBig _     _ _ eq | Yes par | Yes refly = Eq eq (trans par (sym refly))
decUIPloopBig _     _ _ _  | _       | _         = Unit

decUIPloopBase : {A : Set}(decEq : (x y : A) -> Dec (Eq x y))(x : A) -> decUIPloopBig decEq x x Refl
decUIPloopBase decEq x with decEq x x
decUIPloopBase _     _ | Yes par = sym (transSymRefl par)
decUIPloopBase _     _ | No  _   = trivial

decUIPloopAt : {A : Set}(decEq : (x y : A) -> Dec (Eq x y))(x : A)(loop : Eq x x) -> Eq loop Refl
decUIPloopAt decEq x loop with axiomJ (decUIPloopBig decEq) (decUIPloopBase decEq) x x loop
decUIPloopAt decEq x loop | j with decEq x x
decUIPloopAt _     _ loop | j | Yes eq     = trans j (transSymRefl eq)
decUIPloopAt _     _ loop | _ | No  contra = absurd (contra loop)

decUIPloop : {A : Set}(decEq : (x y : A) -> Dec (Eq x y)){x : A}(loop : Eq x x) -> Eq loop Refl
decUIPloop decEq = decUIPloopAt decEq _

decAxiomK : {A : Set}(decEq : (x y : A) -> Dec (Eq x y)){x : A}(P : Eq x x -> Set)(base : P Refl)(loop : Eq x x) -> P loop
decAxiomK decEq P base loop = subst (sym (decUIPloop decEq loop)) P base

{-|
Terms, from Figures 8-1, 8-2, and 9-3

Indexed by number of free variables, which are
represented by DeBuijn indexes.
-}
data Term : Nat -> Set where
  true false : {n : Nat} -> Term n
  if_then_else_ : {n : Nat} -> Term n -> Term n -> Term n -> Term n
  zero : {n : Nat} -> Term n
  succ pred iszero : {n : Nat} -> Term n -> Term n
  var : {n : Nat} -> Fin n -> Term n
  lambda : {n : Nat} -> Type -> Term (S n) -> Term n
  apply : {n : Nat} -> Term n -> Term n -> Term n

{-| Contexts, just a snoc list of types, sincewe are using DeBuijn indexes. -}
data Context : Nat -> Set where
  empty : Context Z
  _,_ : {n : Nat} -> Context n -> Type -> Context (S n)

{-| Retrieve variable type from context.  Functional, instead of relational definition. -}
_!_ : {n : Nat} -> Context n -> Fin n -> Type
empty ! ()
(ctx , x) ! FZ = x
(ctx , x) ! FS i = ctx ! i

{-| Remove a type from context by DeBuijn index. -}
_-_ : {n : Nat} -> Context (S n) -> Fin (S n) -> Context n
(ctx , _ ) - FZ   = ctx
(_   , _ ) - FS i with FinS i
(ctx , ty) - FS i | MkSigma m Refl = (ctx - i), ty

{-| Proposition: a term is a numeric value. -}
data IsNumeric : {n : Nat} -> Term n -> Set where
  NumZ : {n : Nat} -> IsNumeric {n} zero
  NumS : {n : Nat}{nv : Term n} -> IsNumeric nv -> IsNumeric (succ nv)

numericyUnique : {n : Nat}{t : Term n} -> (x : IsNumeric t) -> (y : IsNumeric t) -> Eq x y
numericyUnique NumZ NumZ = Refl
numericyUnique (NumS x) (NumS y) = cong (numericyUnique x y)

{-| Proposition: a term is a value. -}
data IsValue : {n : Nat} -> Term n -> Set where
  ValT : {n : Nat} -> IsValue {n} true
  ValF : {n : Nat} -> IsValue {n} false
  ValN : {n : Nat}{nv : Term n} -> IsNumeric nv -> IsValue nv
  ValL : {ty : Type}{n : Nat}(t : Term (S n)) -> IsValue (lambda ty t)

valuenessUnique : {n : Nat}{t : Term n} -> (x : IsValue t) -> (y : IsValue t) -> Eq x y
valuenessUnique ValT ValT = Refl
valuenessUnique ValT (ValN ())
valuenessUnique ValF ValF = Refl
valuenessUnique ValF (ValN ())
valuenessUnique (ValN ()) ValT
valuenessUnique (ValN ()) ValF
valuenessUnique (ValN x) (ValN y) = cong (numericyUnique x y)
valuenessUnique (ValN ()) (ValL t)
valuenessUnique (ValL t) (ValN ())
valuenessUnique (ValL t) (ValL .t) = Refl

{-| Update a DeBuijn index as it is substituted under a lambda binder. -}
shiftV : {n : Nat} -> Fin (S n) -> Fin n -> Fin (S n)
shiftV FZ x = FS x
shiftV (FS _) FZ = FZ
shiftV (FS i) (FS x) = FS (shiftV i x)

{-| Update term being substituted we go under the lambda binder. -}
shift : {n : Nat} -> Fin (S n) -> Term n -> Term (S n)
shift _ true = true
shift _ false = false
shift n (if t1 then t2 else t3) = if shift n t1 then shift n t2 else shift n t3
shift _ zero = zero
shift n (succ t) = succ (shift n t)
shift n (pred t) = pred (shift n t)
shift n (iszero t) = iszero (shift n t)
shift n (var x) = var (shiftV n x)
shift n (lambda ty t) = lambda ty (shift (FS n) t)
shift n (apply t1 t2) = apply (shift n t1) (shift n t2)

{-|
Strip matching index and update others to account for the one that will be substituted.
-}
substituteV : {n : Nat} -> Fin (S n) -> Fin (S n) -> Maybe (Fin n)
substituteV FZ     FZ = Nothing
substituteV FZ     (FS x) = Just x
substituteV (FS i) _      with FinS i
substituteV (FS _) FZ     | MkSigma _ Refl = Just FZ
substituteV (FS i) (FS x) | MkSigma _ Refl with substituteV i x
substituteV (FS _) (FS _) | MkSigma _ Refl | Nothing = Nothing
substituteV (FS _) (FS _) | MkSigma _ Refl | Just y  = Just (FS y)

{-| The abstraction operation, which involves substitution but also some alpha renaming. -}
substitute : {n : Nat} -> Fin (S n) -> Term n -> Term (S n) -> Term n
substitute i v true = true
substitute i v false = false
substitute i v (if t1 then t2 else t3) =
  if substitute i v t1 then substitute i v t2 else substitute i v t3
substitute i v zero = zero
substitute i v (succ t) = succ (substitute i v t)
substitute i v (pred t) = pred (substitute i v t)
substitute i v (iszero t) = iszero (substitute i v t)
substitute i v (var x) with (substituteV i x)
substitute i v (var x) | Nothing = v
substitute i v (var x) | Just y = var y
substitute i v (lambda ty t) = lambda ty (substitute (FS i) (shift FZ v) t)
substitute i v (apply t1 t2) = apply (substitute i v t1) (substitute i v t2)

{-| Small step evaluation relation -}
data SmallStep : {n : Nat} -> Term n -> Term n -> Set where
  E-IfTrue : {n : Nat}{t2 t3 : Term n} -> SmallStep (if true then t2 else t3) t2
  E-IfFalse : {n : Nat}{t2 t3 : Term n} -> SmallStep (if false then t2 else t3) t3
  E-If : {n : Nat}{t1 t2 t3 u : Term n} -> SmallStep t1 u
      -> SmallStep (if t1 then t2 else t3) (if u then t2 else t3)
  E-Succ : {n : Nat}{t u : Term n} -> SmallStep t u -> SmallStep (succ t) (succ u)
  E-PredZero : {n : Nat} -> SmallStep {n} (pred zero) zero
  E-PredSucc : {n : Nat}{nv : Term n} -> IsNumeric nv -> SmallStep (pred (succ nv)) nv
  E-Pred : {n : Nat}{t u : Term n} -> SmallStep t u -> SmallStep (pred t) (pred u)
  E-IsZeroZero : {n : Nat} -> SmallStep {n} (iszero zero) true
  E-IsZeroSucc : {n : Nat}{nv : Term n} -> IsNumeric nv
              -> SmallStep {n} (iszero (succ nv)) false
  E-IsZero : {n : Nat}{t u : Term n} -> SmallStep t u -> SmallStep (iszero t) (iszero u)
  E-App1 : {n : Nat}{t1 t2 u : Term n} -> SmallStep t1 u -> SmallStep (apply t1 t2) (apply u t2)
  E-App2 : {n : Nat}{v t u : Term n} -> IsValue v -> SmallStep t u
        -> SmallStep (apply v t) (apply v u)
  E-AppAbs : {n : Nat}{v : Term n}{ty : Type}{t : Term (S n)} -> IsValue v
          -> SmallStep (apply (lambda ty t) v) (substitute FZ v t)

{-| Big step evaluation relation -}
data BigStep : {n : Nat} -> Term n -> Term n -> Set where
  B-Val : {n : Nat}{t : Term n} -> IsValue t -> BigStep t t
  B-IfTrue : {n : Nat}{t1 t2 t3 v : Term n}
          -> BigStep t1 true -> BigStep t2 v -> BigStep (if t1 then t2 else t3) v
  B-IfFalse : {n : Nat}{t1 t2 t3 v : Term n}
           -> BigStep t1 false -> BigStep t3 v -> BigStep (if t1 then t2 else t3) v
  B-Succ : {n : Nat}{t1 v : Term n} -> (nv : IsNumeric v)
        -> BigStep t1 v -> BigStep (succ t1) (succ v)
  B-PredZero : {n : Nat}{t1 : Term n} -> BigStep t1 zero -> BigStep (pred t1) zero
  B-PredSucc : {n : Nat}{t1 pv : Term n} -> BigStep t1 (succ pv) -> BigStep (pred t1) pv
  B-IsZeroZero : {n : Nat}{t1 : Term n} -> BigStep t1 zero -> BigStep (iszero t1) true
  B-IsZeroSucc : {n : Nat}{t1 pv : Term n} -> BigStep t1 (succ pv) -> BigStep (iszero t1) false
  B-AppAbs : {n : Nat}{t1 t2 arg v : Term n}{ty : Type}{f : Term (S n)}
          -> BigStep t1 (lambda ty f) -> BigStep t2 arg -> BigStep (substitute FZ arg f) v
          -> BigStep (apply t1 t2) v

{-| If a term evaluates, it does so to a value. -}
bsToValue : {n : Nat}{t v : Term n} -> BigStep t v -> IsValue v
bsToValue (B-Val x) = x
bsToValue (B-IfTrue _ bs) = bsToValue bs
bsToValue (B-IfFalse _ bs) = bsToValue bs
bsToValue (B-Succ nv _) = ValN (NumS nv)
bsToValue (B-PredZero bs) = bsToValue bs
bsToValue (B-PredSucc bs) with bsToValue bs
bsToValue (B-PredSucc _ ) | ValN (NumS x) = ValN x
bsToValue (B-IsZeroZero bs) = ValT
bsToValue (B-IsZeroSucc bs) = ValF
bsToValue (B-AppAbs _ _ bs) = bsToValue bs

{-| If a value evaluates, it does so to itself. -}
bsOnValue : {n : Nat}{t v : Term n} -> IsValue t -> BigStep t v -> Eq t v
bsOnValue ValT (B-Val _) = Refl
bsOnValue ValF (B-Val _) = Refl
bsOnValue (ValN NumZ) (B-Val _) = Refl
bsOnValue (ValN (NumS _)) (B-Val _) = Refl
bsOnValue (ValN (NumS x)) (B-Succ _ bs) = cong (bsOnValue (ValN x) bs)
bsOnValue (ValL _) (B-Val _) = Refl

{-| Taking a small step doesn't change final value. -}
smallBigCons : {n : Nat}{t1 t2 v : Term n}
            -> SmallStep t1 t2 -> BigStep t2 v -> BigStep t1 v
smallBigCons E-IfTrue bs = B-IfTrue (B-Val ValT) bs
smallBigCons E-IfFalse bs = B-IfFalse (B-Val ValF) bs
smallBigCons (E-If ss) (B-Val (ValN ()))
smallBigCons (E-If ss) (B-IfTrue bs bs₁) = B-IfTrue (smallBigCons ss bs) bs₁
smallBigCons (E-If ss) (B-IfFalse bs bs₁) = B-IfFalse (smallBigCons ss bs) bs₁
smallBigCons (E-Succ ss) (B-Val (ValN (NumS x))) = B-Succ x (smallBigCons ss (B-Val (ValN x)))
smallBigCons (E-Succ ss) (B-Succ nv bs) = B-Succ nv (smallBigCons ss bs)
smallBigCons E-PredZero (B-Val zv) = B-PredZero (B-Val zv)
smallBigCons (E-PredSucc nv) bs with bsOnValue (ValN nv) bs
smallBigCons (E-PredSucc nv) bs | Refl = B-PredSucc (B-Succ nv bs)
smallBigCons (E-Pred ss) (B-Val (ValN ()))
smallBigCons (E-Pred ss) (B-PredZero bs) = B-PredZero (smallBigCons ss bs)
smallBigCons (E-Pred ss) (B-PredSucc bs) = B-PredSucc (smallBigCons ss bs)
smallBigCons E-IsZeroZero (B-Val ValT) = B-IsZeroZero (B-Val (ValN NumZ))
smallBigCons E-IsZeroZero (B-Val (ValN ()))
smallBigCons (E-IsZeroSucc x) (B-Val ValF) = B-IsZeroSucc (B-Val (ValN (NumS x)))
smallBigCons (E-IsZeroSucc x) (B-Val (ValN ()))
smallBigCons (E-IsZero ss) (B-Val (ValN ()))
smallBigCons (E-IsZero ss) (B-IsZeroZero bs) = B-IsZeroZero (smallBigCons ss bs)
smallBigCons (E-IsZero ss) (B-IsZeroSucc bs) = B-IsZeroSucc (smallBigCons ss bs)
smallBigCons (E-App1 ss) (B-Val (ValN ()))
smallBigCons (E-App1 ss) (B-AppAbs bs bs₁ bs₂) = B-AppAbs (smallBigCons ss bs) bs₁ bs₂
smallBigCons (E-App2 _ ss) (B-Val (ValN ()))
smallBigCons (E-App2 _ ss) (B-AppAbs bs bs₁ bs₂) = B-AppAbs bs (smallBigCons ss bs₁) bs₂
smallBigCons (E-AppAbs x) bs = B-AppAbs (B-Val (ValL _)) (B-Val x) bs

{-| Typing relationship -}
data Typing : {n : Nat} -> Context n -> Term n -> Type -> Set where
  T-True : {n : Nat}{ctx : Context n} -> Typing ctx true Boolean
  T-False : {n : Nat}{ctx : Context n} -> Typing ctx false Boolean
  T-If : {n : Nat}{ctx : Context n}{t1 t2 t3 : Term n}{ty : Type}
      -> Typing ctx t1 Boolean -> Typing ctx t2 ty -> Typing ctx t3 ty
      -> Typing ctx (if t1 then t2 else t3) ty
  T-Zero : {n : Nat}{ctx : Context n} -> Typing ctx zero Natural
  T-Succ : {n : Nat}{ctx : Context n}{t : Term n}
        -> Typing ctx t Natural -> Typing ctx (succ t) Natural
  T-Pred : {n : Nat}{ctx : Context n}{t : Term n}
        -> Typing ctx t Natural -> Typing ctx (pred t) Natural
  T-IsZero : {n : Nat}{ctx : Context n}{t : Term n} -> Typing ctx t Natural
          -> Typing ctx (iszero t) Boolean
  T-Var : {n : Nat}{ctx : Context n}{x : Fin n}{ty : Type} -> Eq ty (ctx ! x)
       -> Typing ctx (var x) ty
  T-Abs : {n : Nat}{ctx : Context n}{ty1 ty2 : Type}{t : Term (S n)} -> Typing (ctx , ty1) t ty2
       -> Typing ctx (lambda ty1 t) (Arr ty1 ty2)
  T-App : {n : Nat}{ctx : Context n}{t1 t2 : Term n}{ty1 ty2 : Type}
       -> Typing ctx t1 (Arr ty1 ty2) -> Typing ctx t2 ty1
       -> Typing ctx (apply t1 t2) ty2

{-| Generation lemma 1. -}
genTrue : {n : Nat}{ctx : Context n}{ty : Type} -> Typing ctx true ty -> Eq ty Boolean
genTrue T-True = Refl

{-| Generation lemma 2. -}
genFalse : {n : Nat}{ctx : Context n}{ty : Type} -> Typing ctx false ty -> Eq ty Boolean
genFalse T-False = Refl

{-| Generation lemma 3a. -}
genIfCond : {n : Nat}{ctx : Context n}{t1 t2 t3 : Term n}{ty : Type}
         -> Typing ctx (if t1 then t2 else t3) ty
         -> Typing ctx t1 Boolean
genIfCond (T-If x _ _) = x

{-| Generation lemma 3b. -}
genIfThen : {n : Nat}{ctx : Context n}{t1 t2 t3 : Term n}{ty : Type}
         -> Typing ctx (if t1 then t2 else t3) ty
         -> Typing ctx t2 ty
genIfThen (T-If _ x _) = x

{-| Generation lemma 3c. -}
getIfElse : {n : Nat}{ctx : Context n}{t1 t2 t3 : Term n}{ty : Type}
         -> Typing ctx (if t1 then t2 else t3) ty
         -> Typing ctx t3 ty
getIfElse (T-If _ _ x) = x

{-| Generation lemma 4. -}
genZero : {n : Nat}{ctx : Context n}{ty : Type} -> Typing ctx zero ty -> Eq ty Natural
genZero T-Zero = Refl

{-| Generation lemma 5a. -}
genSucc : {n : Nat}{ctx : Context n}{t : Term n}{ty : Type}
       -> Typing ctx (succ t) ty -> Eq ty Natural
genSucc (T-Succ _) = Refl

{-| Generation lemma 5b. -}
genSuccST : {n : Nat}{ctx : Context n}{t : Term n}{ty : Type} -> Typing ctx (succ t) ty
         -> Typing ctx t Natural
genSuccST (T-Succ x) = x

{-| Generation lemma 6a. -}
genPred : {n : Nat}{ctx : Context n}{t : Term n}{ty : Type}
       -> Typing ctx (pred t) ty -> Eq ty Natural
genPred (T-Pred _) = Refl

{-| Generation lemma 6b. -}
genPredST : {n : Nat}{ctx : Context n}{t : Term n}{ty : Type} -> Typing ctx (pred t) ty
         -> Typing ctx t Natural
genPredST (T-Pred x) = x

{-| Generation lemma 7a. -}
genIsZero : {n : Nat}{ctx : Context n}{t : Term n}{ty : Type} -> Typing ctx (iszero t) ty
         -> Eq ty Boolean
genIsZero (T-IsZero _) = Refl

{-| Generation lemma 7b. -}
genIsZeroST : {n : Nat}{ctx : Context n}{t : Term n}{ty : Type} -> Typing ctx (iszero t) ty
           -> Typing ctx t Natural
genIsZeroST (T-IsZero x) = x

{-| Generation lemma 8. -}
genVar : {n : Nat}{ctx : Context n}{ty : Type}{x : Fin n} -> Typing ctx (var x) ty
      -> Eq ty (ctx ! x)
genVar (T-Var prf) = prf

{-| Generation lemma 9. -}
genLambda : {n : Nat}{ctx : Context n}{t : Term (S n)}{ty1 ty2 : Type}
         -> Typing ctx (lambda ty1 t) ty2
         -> Sigma Type \{ ty3 -> Pair (Eq ty2 (Arr ty1 ty3)) (Typing (ctx , ty1) t ty3) }
genLambda (T-Abs x) = eg (MkPair Refl x)

{-| Generation lemma 10. -}
genApp : {n : Nat}{ctx : Context n}{t1 t2 : Term n}{ty1 : Type} -> Typing ctx (apply t1 t2) ty1
         -> Sigma Type \{ ty2 -> Pair (Typing ctx t1 (Arr ty2 ty1)) (Typing ctx t2 ty2) }
genApp (T-App f a) = eg (MkPair f a)

{-| Proposition: In a given context, a term is well-typed (i.e. covered by typing relationhsip. -}
WellTypedIn : {n : Nat} -> Term n -> Context n -> Set
WellTypedIn t ctx = Sigma Type (Typing ctx t)

{-| Proposition: In some context, a term is well-typed. -}
WellTyped : {n : Nat} -> Term n -> Set
WellTyped {n} t = Sigma (Context n) (WellTypedIn t)

{-| Proposition: A term is a immediate subterm of another. -}
data ImmediateSubTerm : {n m : Nat} -> Term n -> Term m -> Set where
  ST-IfCond : {n : Nat}{t1 t2 t3 : Term n} -> ImmediateSubTerm t1 (if t1 then t2 else t3)
  ST-IfThen : {n : Nat}{t1 t2 t3 : Term n} -> ImmediateSubTerm t2 (if t1 then t2 else t3)
  ST-IfElse : {n : Nat}{t1 t2 t3 : Term n} -> ImmediateSubTerm t3 (if t1 then t2 else t3)
  ST-Succ : {n : Nat}{t : Term n} -> ImmediateSubTerm t (succ t)
  ST-Pred : {n : Nat}{t : Term n} -> ImmediateSubTerm t (pred t)
  ST-IsZero : {n : Nat}{t : Term n} -> ImmediateSubTerm t (iszero t)
  ST-Abs : {n : Nat}{t : Term (S n)}{ty : Type} -> ImmediateSubTerm t (lambda ty t)
  ST-AppF : {n : Nat}{t1 t2 : Term n} -> ImmediateSubTerm t1 (apply t1 t2)
  ST-AppA : {n : Nat}{t1 t2 : Term n} -> ImmediateSubTerm t2 (apply t1 t2)

{-| The immediate subterms of a well-typed term are also well-typed. -}
immSubTyped : {n m : Nat}{t : Term n}{t1 : Term m} -> WellTyped t -> ImmediateSubTerm t1 t
           -> WellTyped t1
immSubTyped (MkSigma _ (MkSigma _ T-True)) ()
immSubTyped (MkSigma _ (MkSigma _ T-False)) ()
immSubTyped (MkSigma ctx (MkSigma _ (T-If proof _ _))) ST-IfCond =
  MkSigma ctx (MkSigma Boolean proof)
immSubTyped (MkSigma ctx (MkSigma witness (T-If _ proof _))) ST-IfThen =
  MkSigma ctx (MkSigma witness proof)
immSubTyped (MkSigma ctx (MkSigma witness (T-If _ _ proof))) ST-IfElse =
  MkSigma ctx (MkSigma witness proof)
immSubTyped (MkSigma _ (MkSigma _ T-Zero)) ()
immSubTyped (MkSigma witness (MkSigma _ (T-Succ proof))) ST-Succ =
  MkSigma witness (MkSigma Natural proof)
immSubTyped (MkSigma witness (MkSigma _ (T-Pred proof))) ST-Pred =
  MkSigma witness (MkSigma Natural proof)
immSubTyped (MkSigma witness (MkSigma _ (T-IsZero proof))) ST-IsZero =
  MkSigma witness (MkSigma Natural proof)
immSubTyped (MkSigma _ (MkSigma _ (T-Var _))) ()
immSubTyped (MkSigma witness (MkSigma _ (T-Abs proof))) ST-Abs =
  MkSigma (witness , _) (MkSigma _ proof)
immSubTyped (MkSigma ctx (MkSigma witness (T-App proof _))) ST-AppF =
  MkSigma ctx (MkSigma (Arr _ witness) proof)
immSubTyped (MkSigma ctx (MkSigma _ (T-App _ proof))) ST-AppA =
  MkSigma ctx (MkSigma _ proof)

{-| Proposition: A term is a subterm of another. -}
data SubTerm : {n m : Nat} -> Term n -> Term m -> Set where
  ST-Imm : {n m : Nat}{t1 : Term n}{t : Term m} -> ImmediateSubTerm t1 t -> SubTerm t1 t
  ST-Sub : {n m l : Nat}{t11 : Term n}{t1 : Term m}{t : Term l}
        -> SubTerm t11 t1 -> ImmediateSubTerm t1 t
        -> SubTerm t11 t

{-|
If a property of a term can be pushed to immediate subterms, it can be pushed to all subterms.
-}
subtermsRec : {P : {n : Nat} -> (t : Term n) -> Set}
           -> ({n m : Nat}{t : Term n}{t1 : Term m} -> P t -> ImmediateSubTerm t1 t -> P t1)
           -> {n m : Nat}{t : Term n}{t1 : Term m} -> P t -> SubTerm t1 t -> P t1
subtermsRec isprf tprf (ST-Imm x) = isprf tprf x
subtermsRec isprf tprf (ST-Sub str x) = subtermsRec isprf (isprf tprf x) str

{-| All subterms of well-typed terms are well-typed. -}
subtermsTyped : {n m : Nat}{t : Term n}{t1 : Term m} -> WellTyped t -> SubTerm t1 t
             -> WellTyped t1
subtermsTyped typ = subtermsRec immSubTyped typ

{-| Within some context, the type of a term is unique. -}
typeUnique : {n : Nat}{ctx : Context n}{t : Term n}{ty1 ty2 : Type}
          -> (typ1 : Typing ctx t ty1) -> (typ2 : Typing ctx t ty2)
          -> Eq ty1 ty2
typeUnique T-True T-True = Refl
typeUnique T-False T-False = Refl
typeUnique (T-If typ1 typ2 typ3) (T-If typ4 typ5 typ6) = typeUnique typ2 typ5
typeUnique T-Zero T-Zero = Refl
typeUnique (T-Succ typ1) (T-Succ typ2) = Refl
typeUnique (T-Pred typ1) (T-Pred typ2) = Refl
typeUnique (T-IsZero typ1) (T-IsZero typ2) = Refl
typeUnique (T-Var prf1) (T-Var prf2) = trans prf1 (sym prf2)
typeUnique (T-Abs typ1) (T-Abs typ2) = cong (typeUnique typ1 typ2)
typeUnique (T-App typ1 typ2) (T-App typ3 typ4) with typeUnique typ1 typ3
typeUnique (T-App typ1 typ2) (T-App typ3 typ4) | Refl = Refl

{-| Canonical values of type Boolean. -}
canonBool : {n : Nat}{v : Term n}{ctx : Context n} -> IsValue v -> Typing ctx v Boolean
         -> Either (Eq v (true {n})) (Eq v (false {n}))
canonBool ValT typ = Left Refl
canonBool ValF typ = Right Refl
canonBool (ValN NumZ) ()
canonBool (ValN (NumS x)) ()
canonBool (ValL t) ()

{-| Canonical values of type Nat. -}
canonNat : {n : Nat}{v : Term n}{ctx : Context n}
        -> IsValue v -> Typing ctx v Natural -> IsNumeric v
canonNat ValT ()
canonNat ValF ()
canonNat (ValN nvp) typ = nvp
canonNat (ValL t) ()

{-| Canonical function values. -}
canonArr : {n : Nat}{v : Term n}{ctx : Context n}{ty1 ty2 : Type}
        -> IsValue v -> Typing ctx v (Arr ty1 ty2)
        -> Sigma (Term (S n)) \{ t1 -> Eq v (lambda ty1 t1) }
canonArr ValT ()
canonArr ValF ()
canonArr (ValN NumZ) ()
canonArr (ValN (NumS x)) ()
canonArr (ValL t) (T-Abs typ) = MkSigma t Refl

{-| Terms with no free variables are closed. -}
Closed : Set
Closed = Term Z

{-| Closed terms (no free variables) are either values or can have a step applied. -}
progress : (t : Closed){ty : Type} -> Typing empty t ty
        -> Either (IsValue t) (Sigma Closed \{ u -> SmallStep t u })
progress true _ = Left ValT
progress false _ = Left ValF
progress (if t1 then t2 else t3) (T-If typ1 typ2 typ3) with (progress t1 typ1)
progress (if _  then t2 else t3) (T-If typ1 _    _   ) | Left vp with (canonBool vp typ1)
progress (if _  then t2 else _ ) (T-If _    _    _   ) | Left _ | Left Refl =
  Right (MkSigma t2 E-IfTrue)
progress (if _  then _  else t3) (T-If _    _    _   ) | Left _  | Right Refl =
  Right (MkSigma t3 E-IfFalse)
progress (if _  then t2 else t3) (T-If _    _    _   ) | Right (MkSigma witness proof) =
  Right (MkSigma (if witness then t2 else t3) (E-If proof))
progress zero _ = Left (ValN NumZ)
progress (succ t) (T-Succ typ) with (progress t typ)
progress (succ _) (T-Succ typ) | Left vp = Left (ValN (NumS (canonNat vp typ)))
progress (succ _) (T-Succ _  ) | Right (MkSigma witness proof) =
  Right (MkSigma (succ witness) (E-Succ proof))
progress (pred t) (T-Pred typ) with (progress t typ)
progress (pred _) (T-Pred typ) | Left vp with (canonNat vp typ)
progress (pred _) (T-Pred _  ) | Left _  | NumZ = Right (eg E-PredZero)
progress (pred _) (T-Pred _  ) | Left _  | NumS isnum = Right (eg (E-PredSucc isnum))
progress (pred _) (T-Pred _  ) | Right (MkSigma witness proof) =
  Right (MkSigma (pred witness) (E-Pred proof))
progress (iszero t) (T-IsZero typ) with (progress t typ)
progress (iszero t) (T-IsZero typ) | Left vp with (canonNat vp typ)
progress (iszero _) (T-IsZero _  ) | Left _  | NumZ = Right (MkSigma true E-IsZeroZero)
progress (iszero _) (T-IsZero _  ) | Left _  | NumS nvp = Right (MkSigma false (E-IsZeroSucc nvp))
progress (iszero _) (T-IsZero _  ) | Right (MkSigma witness proof) =
  Right (MkSigma (iszero witness) (E-IsZero proof))
progress (var ()) _
progress (lambda _ t) _ = Left (ValL t)
progress (apply t1 t2) (T-App typ1 typ2) with (progress t1 typ1)
progress (apply t1 t2) (T-App typ1 typ2) | Left vp1 with (progress t2 typ2)
progress (apply _  t2) (T-App typ1 _   ) | Left vp1 | Left vp2 with (canonArr vp1 typ1)
progress (apply _  t2) (T-App _    _   ) | Left _   | Left vp2 | MkSigma witness Refl =
  Right (MkSigma (substitute FZ t2 witness) (E-AppAbs vp2))
progress (apply t1 _ ) (T-App _    _   ) | Left vp1 | Right (MkSigma witness proof) =
  Right (MkSigma (apply t1 witness) (E-App2 vp1 proof))
progress (apply _  t2) (T-App _    _   ) | Right (MkSigma witness proof) =
  Right (MkSigma (apply witness t2) (E-App1 proof))

{-| Weakening specifically for variables. -}
succVTyping : {n : Nat}{ctx : Context n}{i : Fin n}{ty1 ty2 : Type}
            -> Typing ctx (var i) ty1 -> Typing (ctx , ty2) (var (FS i)) ty1
succVTyping (T-Var prf) = T-Var prf

{-| Shifting a variable preserves typing. -}
shiftVPreservation : {n : Nat}{ty : Type}
                   -> (ctx : Context (S n)) -> (i : Fin (S n)) -> (x : Fin n)
                   -> Typing (ctx - i) (var x) ty -> Typing ctx (var (shiftV i x)) ty
shiftVPreservation (_ , _)  FZ    _      (T-Var prf) = T-Var prf
shiftVPreservation (_ , _) (FS i) x      (T-Var prf) with FinS i
shiftVPreservation (_ , _) (FS _) FZ     (T-Var prf) | MkSigma _ Refl =
  T-Var prf
shiftVPreservation (_ , _) (FS _) (FS _) (T-Var prf) | MkSigma _ Refl =
  succVTyping (shiftVPreservation _ _ _ (T-Var prf))

{-| Shifting a term preserves typing. -}
shiftPreservation : {n : Nat}{t : Term n}{ty : Type}
                 -> (ctx : Context (S n)) -> (i : Fin (S n)) -> Typing (ctx - i) t ty
                 -> Typing ctx (shift i t) ty
shiftPreservation _ _ T-True               = T-True
shiftPreservation _ _ T-False              = T-False
shiftPreservation _ _ (T-If typ typt typf) =
  T-If (shiftPreservation _ _ typ) (shiftPreservation _ _ typt) (shiftPreservation _ _ typf)
shiftPreservation _ _ T-Zero               = T-Zero
shiftPreservation _ _ (T-Succ typ)         = T-Succ (shiftPreservation _ _ typ)
shiftPreservation _ _ (T-Pred typ)         = T-Pred (shiftPreservation _ _ typ)
shiftPreservation _ _ (T-IsZero typ)       = T-IsZero (shiftPreservation _ _ typ)
shiftPreservation _ _ (T-Var prf)          = shiftVPreservation _ _ _ (T-Var prf)
shiftPreservation _ i (T-Abs _)            with shiftPreservation (_ , _) (FS i)
shiftPreservation _ i (T-Abs _)            | sh with FinS i
shiftPreservation _ _ (T-Abs typ)          | sh | MkSigma _ Refl = T-Abs (sh typ)
shiftPreservation _ _ (T-App typ1 typ2)    =
  T-App (shiftPreservation _ _ typ1) (shiftPreservation _ _ typ2)

{-| Veriable substitution recursion. -}
succSubV : {n : Nat}{y : Fin n} -> (i : Fin (S n)) -> (x : Fin (S n))
         -> Eq (substituteV i x) (Just y) -> Eq (substituteV (FS i) (FS x)) (Just (FS y))
succSubV i _ _    with FinS i
succSubV i x _    | MkSigma _ Refl with substituteV i x
succSubV _ _ ()   | MkSigma _ Refl | Nothing
succSubV _ _ Refl | MkSigma _ Refl | Just _ = Refl

{-| Substituting a variable removes it. -}
subVEqN : {n : Nat} -> (i : Fin (S n)) -> Eq (substituteV i i) Nothing
subVEqN FZ     = Refl
subVEqN (FS i) with FinS i
subVEqN (FS i) | MkSigma _ Refl with subVEqN i
subVEqN (FS i) | MkSigma _ Refl | ind with substituteV i i
subVEqN (FS _) | MkSigma _ Refl | ind | Nothing = Refl
subVEqN (FS _) | MkSigma _ Refl | ()  | Just _

{-| Substitution applied to a variable term preserves typing. -}
subVPreservation : {n : Nat}{ty : Type}
                -> (i : Fin (S n))
                -> (ctx : Context (S n)) -> (x : Fin (S n)) -> Typing ctx (var x) ty
                -> Either (Eq x i)
                          (Sigma (Fin n) \{y -> Pair (Eq (substituteV i x) (Just y))
                                                     (Typing (ctx - i) (var y) ty)})
subVPreservation FZ     _         FZ     _     = Left Refl
subVPreservation FZ     (_   , _) (FS x) (T-Var prf) = Right (MkSigma x (MkPair Refl (T-Var prf)))
subVPreservation (FS i) (_   , _) _      (T-Var prf) with FinS i
subVPreservation (FS _) (_   , _) FZ     (T-Var prf) | MkSigma _ Refl = Right (MkSigma _ (MkPair Refl (T-Var prf)))
subVPreservation (FS i) (ctx , _) (FS x) (T-Var prf) | MkSigma _ Refl with subVPreservation i ctx x (T-Var prf)
subVPreservation (FS _) (_   , _) (FS _) (T-Var prf) | MkSigma _ Refl | Left Refl = Left Refl
subVPreservation (FS i) (_   , _) (FS x) (T-Var prf) | MkSigma _ Refl | Right (MkSigma _ (MkPair _    typ)) with substituteV i x
subVPreservation (FS _) (_   , _) (FS _) (T-Var prf) | MkSigma _ Refl | Right (MkSigma _ (MkPair ()   typ)) | Nothing
subVPreservation (FS _) (_   , _) (FS _) (T-Var prf) | MkSigma _ Refl | Right (MkSigma _ (MkPair Refl typ)) | Just _  =
  Right (MkSigma _ (MkPair Refl (succVTyping typ)))

{-| Substitution preserves typing. -}
subPreservation : {n : Nat}{ty : Type}{t2 : Term n} -> (ctx : Context (S n)) -> (t1 : Term (S n))
               -> Typing ctx t1 ty -> (i : Fin (S n)) -> Typing (ctx - i) t2 (ctx ! i)
               -> Typing (ctx - i) (substitute i t2 t1) ty
subPreservation _   true            T-True           _ _    = T-True
subPreservation _   false           T-False          _ _    = T-False
subPreservation ctx (if t1 then t2 else t3) (T-If typc typt typf) i typ2 =
  T-If (subPreservation ctx t1 typc i typ2)
       (subPreservation ctx t2 typt i typ2)
       (subPreservation ctx t3 typf i typ2)
subPreservation _   zero            T-Zero           _ _    = T-Zero
subPreservation ctx (succ _)        (T-Succ typ)     i typ2 =
  T-Succ (subPreservation ctx _ typ i typ2)
subPreservation ctx (pred _)        (T-Pred typ)     i typ2 =
  T-Pred (subPreservation ctx _ typ i typ2)
subPreservation ctx (iszero _)      (T-IsZero typ)   i typ2 =
  T-IsZero (subPreservation ctx _ typ i typ2)
subPreservation ctx (var x)         (T-Var prf)      i _    with subVPreservation i ctx x (T-Var prf)
subPreservation _   (var i)         (T-Var prf)      _ _    | Left Refl with subVEqN i
subPreservation _   (var i)         (T-Var prf)      _ _    | Left Refl | _  with substituteV i i
subPreservation _   (var _)         (T-Var Refl)     _ typ  | Left Refl | _  | Nothing = typ
subPreservation _   (var _)         (T-Var prf)      _ _    | Left Refl | () | Just _
subPreservation _   (var x)         (T-Var prf)      i _    | Right (MkSigma _ (MkPair _    _  )) with substituteV i x
subPreservation _   (var _)         (T-Var prf)      _ _    | Right (MkSigma _ (MkPair ()   _  )) | Nothing
subPreservation _   (var _)         (T-Var prf)      _ _    | Right (MkSigma _ (MkPair Refl typ)) | Just _ = typ
subPreservation ctx (lambda ty1 t1) (T-Abs typ)      i _
                with subPreservation (ctx , ty1) t1 typ (FS i)
subPreservation _   (lambda _   _)  (T-Abs _)        i _
                | sub with FinS i
subPreservation ctx (lambda ty1 _)  (T-Abs typ)      i typ2
                | sub | MkSigma _ Refl = T-Abs (sub (shiftPreservation ((ctx - i), ty1) FZ typ2))
subPreservation ctx (apply t1 t2)   (T-App typ typ1) i typ2 =
  T-App (subPreservation ctx t1 typ i typ2) (subPreservation ctx t2 typ1 i typ2)

{-| Small-step evaluation preseves typing. -}
preservation : {n : Nat}{ctx : Context n}{t1 t2 : Term n}{ty : Type}
            -> SmallStep t1 t2 -> Typing ctx t1 ty -> Typing ctx t2 ty
preservation E-IfTrue          (T-If _ typ _)            = typ
preservation E-IfFalse         (T-If _ _ typ)            = typ
preservation (E-If ss)         (T-If typ typt typf)      = T-If (preservation ss typ) typt typf
preservation (E-Succ ss)       (T-Succ typ)              = T-Succ (preservation ss typ)
preservation E-PredZero        (T-Pred typ)              = typ
preservation (E-PredSucc nv)   (T-Pred (T-Succ typ))     = typ
preservation (E-Pred ss)       (T-Pred typ)              = T-Pred (preservation ss typ)
preservation E-IsZeroZero      (T-IsZero typ)            = T-True
preservation (E-IsZeroSucc nv) (T-IsZero typ)            = T-False
preservation (E-IsZero ss)     (T-IsZero typ)            = T-IsZero (preservation ss typ)
preservation (E-App1 ss)       (T-App typ1 typ2)         = T-App (preservation ss typ1) typ2
preservation (E-App2 v ss)     (T-App typ1 typ2)         = T-App typ1 (preservation ss typ2)
preservation (E-AppAbs v)      (T-App (T-Abs typ1) typ2) = subPreservation (_ , _) _ typ1 FZ typ2

{-| Big-step evaluation preserves typing. -}
preservationBig : {n : Nat}{ctx : Context n}{t v : Term n}{ty : Type}
               -> BigStep t v -> Typing ctx t ty -> Typing ctx v ty
preservationBig (B-Val _) typ = typ
preservationBig (B-IfTrue bs bs₁) (T-If typ typ₁ typ₂) = preservationBig bs₁ typ₁
preservationBig (B-IfFalse bs bs₁) (T-If typ typ₁ typ₂) = preservationBig bs₁ typ₂
preservationBig (B-Succ nv bs) (T-Succ typ) = T-Succ (preservationBig bs typ)
preservationBig (B-PredZero bs) (T-Pred typ) = preservationBig bs typ
preservationBig (B-PredSucc bs) (T-Pred typ) = genSuccST (preservationBig bs typ)
preservationBig (B-IsZeroZero bs) (T-IsZero typ) = T-True
preservationBig (B-IsZeroSucc bs) (T-IsZero typ) = T-False
preservationBig (B-AppAbs bs bs₁ bs₂) (T-App typ typ₁) with preservationBig bs typ
preservationBig (B-AppAbs bs bs₁ bs₂) (T-App typ typ₁) | T-Abs typ2 =
  preservationBig bs₂ (subPreservation (_ , _) _ typ2 FZ (preservationBig bs₁ typ₁))

Halts : (t : Closed) -> Set
Halts t = Sigma Closed (BigStep t)

{- Reduction as if of an atomic type, without preservation. -}
record NSReduce (ty : Type) (t : Closed) : Set where
 constructor MkNSReduce
 field
  typing : Typing empty t ty
  halts  : Halts t

{- Reduction and reduction preservation across substitution. -}
Reduces : (ty : Type) -> (t : Closed) -> Set
Reduces (Arr tya tys) t = Pair (NSReduce (Arr tya tys) t)
                               ((a : Closed) -> Reduces tya a -> Reduces tys (apply t a))
Reduces ty t = NSReduce ty t

reducesTypes : {t : Closed} -> (ty : Type) -> Reduces ty t -> Typing empty t ty
reducesTypes Boolean (MkNSReduce typing _) = typing
reducesTypes Natural (MkNSReduce typing _) = typing
reducesTypes (Arr _ _) (MkPair (MkNSReduce typing _) _) = typing

reducesHalts : {t : Closed} -> (ty : Type) -> Reduces ty t -> Halts t
reducesHalts Boolean (MkNSReduce _ halts) = halts
reducesHalts Natural (MkNSReduce _ halts) = halts
reducesHalts (Arr _ _) (MkPair (MkNSReduce _ halts) _) = halts

{-# TERMINATING #-}
{- Direct progress on big-step. -}
progressBig : (t : Closed){ty : Type} -> Typing empty t ty -> Sigma Closed (BigStep t)
progressBig true _ = MkSigma true (B-Val ValT)
progressBig false _ = MkSigma false (B-Val ValF)
progressBig (if t1 then t₁ else t₂) (T-If typ typ₁ typ₂) with progressBig t1 typ
progressBig (if t1 then t₁ else t₂) (T-If typ typ₁ typ₂) | MkSigma _ bsc
            with canonBool (bsToValue bsc) (preservationBig bsc typ)
progressBig (if t1 then t₁ else t₂) (T-If typ typ₁ typ₂) | MkSigma _ bsc
            | Left Refl with progressBig t₁ typ₁
progressBig (if t1 then t₁ else t₂) (T-If typ typ₁ typ₂) | MkSigma _ bsc
            | Left Refl | MkSigma v bsb = MkSigma v (B-IfTrue bsc bsb)
progressBig (if t1 then t₁ else t₂) (T-If typ typ₁ typ₂) | MkSigma _ bsc
            | Right Refl with progressBig t₂ typ₂
progressBig (if t1 then t₁ else t₂) (T-If typ typ₁ typ₂) | MkSigma _ bsc
            | Right Refl | MkSigma v bsb = MkSigma v (B-IfFalse bsc bsb)
progressBig zero typ = MkSigma zero (B-Val (ValN NumZ))
progressBig (succ t) (T-Succ typ) with progressBig t typ
progressBig (succ _) (T-Succ typ) | MkSigma witness proof =
  MkSigma (succ witness) (B-Succ (canonNat (bsToValue proof) (preservationBig proof typ)) proof)
progressBig (pred t) (T-Pred typ) with progressBig t typ
... | MkSigma _ proof with canonNat (bsToValue proof) (preservationBig proof typ)
...                   | NumZ = MkSigma zero (B-PredZero proof)
...                   | NumS _ = MkSigma _ (B-PredSucc proof)
progressBig (iszero t) (T-IsZero typ) with progressBig t typ
... | MkSigma _ bs with canonNat (bsToValue bs) (preservationBig bs typ)
progressBig (iszero _) (T-IsZero _  ) | MkSigma _ bs | NumZ = MkSigma true (B-IsZeroZero bs)
progressBig (iszero _) (T-IsZero _  ) | MkSigma _ bs | NumS _ = MkSigma false (B-IsZeroSucc bs)
progressBig (var ()) typ
progressBig (lambda x t) typ = MkSigma (lambda x t) (B-Val (ValL t))
progressBig (apply tf tx) (T-App typf typx) with progressBig tf typf
progressBig (apply _  tx) (T-App typf typx) | MkSigma _ bsl with progressBig tx typx
...                                         | MkSigma arg bsa with preservationBig bsl typf
...                                         | typl with canonArr (bsToValue bsl) typl
...                                         | MkSigma bdy Refl with genLambda typl
... | MkSigma _ (MkPair _ typb) with progressBig (substitute FZ arg bdy)
                                                 (subPreservation (empty , _) bdy typb FZ
                                                                  (preservationBig bsa typx))
...                             | MkSigma v bse = MkSigma v (B-AppAbs bsl bsa bse)

{-# TERMINATING #-}
progressBigAlt : (t : Closed){ty : Type} -> Typing empty t ty -> Sigma Closed (BigStep t)
progressBigAlt t typ with progress t typ
progressBigAlt t typ | Left v = MkSigma t (B-Val v)
progressBigAlt _ typ | Right (MkSigma t2 ss) with progressBigAlt t2 (preservation ss typ)
progressBigAlt _ typ | Right (MkSigma t2 ss) | MkSigma v bs = MkSigma v (smallBigCons ss bs)

{-| Evaluation loop for small-step evaluation. -}
{-# TERMINATING #-}
evalSmall : {ty : Type} -> (t : Closed) -> Typing empty t ty
        -> Sigma Closed \{ v -> Pair (IsValue v) (Typing empty v ty) }
evalSmall t typ with progress t typ
evalSmall t typ | Left v                  = MkSigma t (MkPair v typ)
evalSmall t typ | Right (MkSigma next ss) = evalSmall next (preservation ss typ)

toAgdaType : Type -> Set
toAgdaType Boolean = Bool
toAgdaType Natural = Nat
toAgdaType (Arr x y) = toAgdaType x -> toAgdaType y

Embed : Type -> Set
Embed ty = toAgdaType ty -> Sigma Closed \{ v -> Pair (IsValue v) (Typing empty v ty) }

embedBool : Embed Boolean
embedBool F = MkSigma _ (MkPair ValF T-False)
embedBool T = MkSigma _ (MkPair ValT T-True)

embedNat : Embed Natural
embedNat Z = MkSigma _ (MkPair (ValN NumZ) T-Zero)
embedNat (S x) with embedNat x
embedNat (S x) | MkSigma _ (MkPair ValT ())
embedNat (S x) | MkSigma _ (MkPair ValF ())
embedNat (S x) | MkSigma witness (MkPair (ValN nv) snd) =
  MkSigma (succ witness) (MkPair (ValN (NumS nv)) (T-Succ snd))
embedNat (S x) | MkSigma _ (MkPair (ValL t) ())

{-| Only embeddable function type, and that it by pre-computation. -}
embedArrBool : {ty2 : Type} -> Embed ty2 -> Embed (Arr Boolean ty2)
embedArrBool emRes f with emRes (f T)
embedArrBool emRes f | MkSigma ft (MkPair _ typt) with emRes (f F)
embedArrBool _     _ | MkSigma ft (MkPair _ typt) | MkSigma ff (MkPair _ typf) =
  MkSigma (lambda _ (if var FZ then shift FZ ft else shift FZ ff))
          (MkPair (ValL _)
                  (T-Abs (T-If (T-Var Refl)
                               (shiftPreservation _ FZ typt)
                               (shiftPreservation _ FZ typf))))

Extract : Type -> Set
Extract ty = (v : Closed) -> IsValue v -> Typing empty v ty -> toAgdaType ty

extractBool : Extract Boolean
extractBool _ ValT            _ = T
extractBool _ ValF            _ = F
extractBool _ (ValN NumZ)     ()
extractBool _ (ValN (NumS _)) ()
extractBool _ (ValL _)        ()

extractNat : Extract Natural
extractNat _        ValT            ()
extractNat _        ValF            ()
extractNat _        (ValN NumZ)     _            = Z
extractNat (succ n) (ValN (NumS x)) (T-Succ typ) = S (extractNat n (ValN x) typ)
extractNat _        (ValL t)        ()

extractArr : {ty1 ty2 : Type} -> Embed ty1 -> Extract ty2 -> Extract (Arr ty1 ty2)
extractArr _     _       _ ValT            ()  _
extractArr _     _       _ ValF            ()  _
extractArr _     _       _ (ValN NumZ)     ()  _
extractArr _     _       _ (ValN (NumS x)) ()  _
extractArr embed extract _ (ValL t)        typ av with embed av
... | MkSigma v (MkPair _ typ2) with evalSmall (apply (lambda _ t) v) (T-App typ typ2)
... | MkSigma res (MkPair isv typ3) = extract res isv typ3
